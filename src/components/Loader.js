import React from "react";

export const Loader = () => (
  <div className="spinner-border text-primary">
    <span className="sr-only"> Loading... </span>{" "}
  </div>
);
